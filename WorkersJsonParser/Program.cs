﻿using System;
using WorkersJsonParser.Accessory;
using WorkersJsonParser.Commands;
using WorkersJsonParser.Models;

namespace WorkersJsonParser
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var commandsHandler = new CommandsWorker();
            if (args.Length == 0 ||
                !Enum.TryParse(args[0].Replace("-", string.Empty), true, out TypesCommand command))
            {
                Console.WriteLine("Incorrect command");
                return;
            }

            commandsHandler.Invoke(command, args.ArgsToDictionary());
        }
    }
}