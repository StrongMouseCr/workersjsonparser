﻿using System.Collections.Generic;

namespace WorkersJsonParser.Commands
{
    public interface ICommandHandler
    {
        void Invoke(IDictionary<string, string> args);
    }
}