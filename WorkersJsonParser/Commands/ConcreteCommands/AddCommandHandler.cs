﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using WorkersJsonParser.Accessory;
using WorkersJsonParser.Models;

namespace WorkersJsonParser.Commands.ConcreteCommands
{
    [CommandHandler(TypesCommand.Add)]
    public class AddCommandHandler : ICommandHandler
    {
        public void Invoke(IDictionary<string, string> args)
        {
            var workers = JsonSerializer.Deserialize<List<WorkerInfo>>(File.ReadAllText(Paths.OutputPath));
            if (workers is null)
                return;

            var id = 0;
            if (workers.Any())
                id = workers.Max(w => w.Id) + 1;

            workers.Add(new WorkerInfo(id, args));
            File.WriteAllText(Paths.OutputPath,
                JsonSerializer.Serialize(workers, new JsonSerializerOptions {WriteIndented = true}));
        }
    }
}