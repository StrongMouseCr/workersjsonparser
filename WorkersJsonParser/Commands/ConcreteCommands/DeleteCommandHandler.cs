﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using WorkersJsonParser.Accessory;
using WorkersJsonParser.Models;

namespace WorkersJsonParser.Commands.ConcreteCommands
{
    [CommandHandler(TypesCommand.Delete)]
    public class DeleteCommandHandler : ICommandHandler
    {
        public void Invoke(IDictionary<string, string> args)
        {
            var workers = JsonSerializer.Deserialize<List<WorkerInfo>>(File.ReadAllText(Paths.OutputPath));
            if (workers is null)
                return;

            var id = int.Parse(args[typeof(WorkerInfo).GetDisplayName(nameof(WorkerInfo.Id))!]);
            if (workers.Any(w => w.Id == id))
            {
                workers.RemoveAt(id);
                File.WriteAllText(Paths.OutputPath,
                    JsonSerializer.Serialize(workers, new JsonSerializerOptions {WriteIndented = true}));
            }
            else
            {
                throw new Exception($"Error: worker with id({id}) not found");
            }
        }
    }
}