﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using WorkersJsonParser.Accessory;
using WorkersJsonParser.Models;

namespace WorkersJsonParser.Commands.ConcreteCommands
{
    [CommandHandler(TypesCommand.Update)]
    public class UpdateCommandHandler : ICommandHandler
    {
        public void Invoke(IDictionary<string, string> args)
        {
            var workers = JsonSerializer.Deserialize<List<WorkerInfo>>(File.ReadAllText(Paths.OutputPath));
            var id = int.Parse(args[typeof(WorkerInfo).GetDisplayName(nameof(WorkerInfo.Id))!]);
            var updatedWorker = workers?.FirstOrDefault(w => w.Id == id);
            if (updatedWorker is null)
                throw new Exception($"Error: worker with id({id}) not found");

            var propertyKey = args.Keys.ToArray()[1];
            updatedWorker.SetValueByDisplayName(propertyKey, args[propertyKey]);
            File.WriteAllText(Paths.OutputPath,
                JsonSerializer.Serialize(workers, new JsonSerializerOptions {WriteIndented = true}));
        }
    }
}