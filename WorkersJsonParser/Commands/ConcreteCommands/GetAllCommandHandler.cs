﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.Json;
using WorkersJsonParser.Accessory;
using WorkersJsonParser.Models;

namespace WorkersJsonParser.Commands.ConcreteCommands
{
    [CommandHandler(TypesCommand.GetAll)]
    public class GetAllCommandHandler : ICommandHandler
    {
        public void Invoke(IDictionary<string, string> args)
        {
            var workers = JsonSerializer.Deserialize<List<WorkerInfo>>(File.ReadAllText(Paths.OutputPath));
            if (workers is null)
                return;

            foreach (var worker in workers)
                Console.WriteLine($"Id = {worker.Id}, " +
                                  $"FirstName = {worker.FirstName}, " +
                                  $"LastName = {worker.LastName}, " +
                                  $"SalaryPerHour = {worker.SalaryPerHour.ToString(CultureInfo.InvariantCulture)}");
        }
    }
}