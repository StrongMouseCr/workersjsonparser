﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.Json;
using WorkersJsonParser.Accessory;
using WorkersJsonParser.Models;

namespace WorkersJsonParser.Commands.ConcreteCommands
{
    [CommandHandler(TypesCommand.Get)]
    public class GetCommandHandler : ICommandHandler
    {
        public void Invoke(IDictionary<string, string> args)
        {
            var workers = JsonSerializer.Deserialize<List<WorkerInfo>>(File.ReadAllText(Paths.OutputPath));
            var id = int.Parse(args[typeof(WorkerInfo).GetDisplayName(nameof(WorkerInfo.Id))!]);
            var worker = workers?.FirstOrDefault(w => w.Id == id);
            if (worker is null)
                throw new Exception($"Error: worker with id({id}) not found");

            Console.WriteLine(
                $"Id = {worker.Id}, " +
                $"FirstName = {worker.FirstName}, " +
                $"LastName = {worker.LastName}, " +
                $"SalaryPerHour = {worker.SalaryPerHour.ToString(CultureInfo.InvariantCulture)}");
        }
    }
}