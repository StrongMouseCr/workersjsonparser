﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using WorkersJsonParser.Accessory;
using WorkersJsonParser.Models;

namespace WorkersJsonParser.Commands
{
    public class CommandsWorker
    {
        private readonly Dictionary<TypesCommand, ICommandHandler> _handlers = new();

        public CommandsWorker()
        {
            var types = Assembly.GetExecutingAssembly().GetTypes()
                .Where(t => t.IsDefined(typeof(CommandHandlerAttribute)));

            foreach (var type in types)
            {
                var instance = (ICommandHandler) Activator.CreateInstance(type)!;
                var command = instance.GetType().GetCommandType();
                if (command is not null)
                    SetHandler((TypesCommand) command, instance);
            }
        }

        private void SetHandler(TypesCommand command, ICommandHandler handler)
        {
            _handlers.Add(command, handler);
        }

        public void Invoke(TypesCommand command, Dictionary<string, string> args)
        {
            try
            {
                _handlers[command].Invoke(args);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Fail with run {command} command:\n{e.Message}");
            }
        }
    }
}