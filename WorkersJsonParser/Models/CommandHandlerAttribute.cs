﻿using System;

namespace WorkersJsonParser.Models
{
    public class CommandHandlerAttribute : Attribute
    {
        public CommandHandlerAttribute(TypesCommand typeCommand)
        {
            TypeCommand = typeCommand;
        }

        public TypesCommand TypeCommand { get; }
    }
}