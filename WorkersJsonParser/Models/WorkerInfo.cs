﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using WorkersJsonParser.Accessory;

namespace WorkersJsonParser.Models
{
    public class WorkerInfo
    {
        public WorkerInfo()
        {
        }

        public WorkerInfo(int id, IDictionary<string, string> args)
        {
            Id = id;
            FirstName = args[GetType().GetDisplayName(nameof(FirstName))!];
            LastName = args[GetType().GetDisplayName(nameof(LastName))!];
            SalaryPerHour = decimal.Parse(args[GetType().GetDisplayName(nameof(SalaryPerHour))!],
                CultureInfo.InvariantCulture);
        }

        public WorkerInfo(int id, string firstName, string lastName, decimal salary)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            SalaryPerHour = salary;
        }

        [DisplayName("id")] public int Id { get; set; }

        [DisplayName("firstname")] public string FirstName { get; set; }

        [DisplayName("lastname")] public string LastName { get; set; }

        [DisplayName("salary")] public decimal SalaryPerHour { get; set; }
    }
}