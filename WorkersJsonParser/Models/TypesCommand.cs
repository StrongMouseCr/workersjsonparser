﻿namespace WorkersJsonParser.Models
{
    public enum TypesCommand
    {
        Add,
        Update,
        Get,
        GetAll,
        Delete
    }
}