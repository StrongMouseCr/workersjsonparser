﻿using System;

namespace WorkersJsonParser.Accessory
{
    public static class Paths
    {
        public static string GeneralPath => AppContext.BaseDirectory;
        public static string OutputPath => $"{GeneralPath}Output/Result.json";
    }
}