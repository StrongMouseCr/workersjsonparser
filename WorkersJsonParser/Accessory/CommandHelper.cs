﻿using System.Collections.Generic;

namespace WorkersJsonParser.Accessory
{
    public static class CommandHelper
    {
        public static Dictionary<string, string> ArgsToDictionary(this string[] args)
        {
            var argDictionary = new Dictionary<string, string>();
            foreach (var arg in args)
            {
                var pair = ArgToKeyValuePair(arg);
                if (!string.IsNullOrEmpty(pair.Key) && !string.IsNullOrEmpty(pair.Value))
                    argDictionary.Add(pair.Key, pair.Value);
            }

            return argDictionary;
        }

        private static KeyValuePair<string, string> ArgToKeyValuePair(string arg)
        {
            var pair = arg.Split(':');
            var key = string.Empty;
            var val = string.Empty;
            if (pair.Length > 1)
            {
                key = pair[0].ToLower();
                val = pair[1];
            }

            return new KeyValuePair<string, string>(key, val);
        }
    }
}