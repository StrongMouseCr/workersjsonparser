﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using WorkersJsonParser.Models;

namespace WorkersJsonParser.Accessory
{
    public static class AttributeExtensions
    {
        public static string? GetDisplayName(this Type type, string parameterName)
        {
            return type.GetProperty(parameterName)?.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName;
        }

        public static TypesCommand? GetCommandType(this Type type)
        {
            return type.GetCustomAttribute<CommandHandlerAttribute>()?.TypeCommand;
        }

        public static void SetValueByDisplayName<T>(this T obj, string propertyName, string value)
        {
            foreach (var propertyInfo in typeof(T).GetProperties())
            {
                var displayName = propertyInfo.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName;
                if (displayName == propertyName)
                    propertyInfo.SetValue(obj,
                        Convert.ChangeType(value, propertyInfo.PropertyType, CultureInfo.InvariantCulture), null);
            }
        }
    }
}