# WorkersJsonParser
Тестовое задание для управление списком работников

---

# Модели

### Модель работника
```csharp
[DisplayName("id")]
public int Id { get; set; }
[DisplayName("firstname")]
public string FirstName { get; set; }
[DisplayName("lastname")]
public string LastName { get; set; }
[DisplayName("salary")]
public decimal SalaryPerHour { get; set; }
```

---

# Команды

### add - добавление записи о сотруднике
Формат использования: -add FirstName:{FirstName:string} LastName:{LastName:string} Salary:{Salary:decimal}

Пример: -add FirstName:John LastName:Doe Salary:100.50

Возможные ошибки: Нет одного из параметров

### update - обновление записи о сотруднике
Формат использования: -update Id:{Id:int} {NameChangedField:string}:{ContentChangedField:string}, где
NameChangedField - enum: FirstName, LastName, Salary

Пример: -update Id:123 FirstName:James

Возможные ошибки: Нет сотрудника с заданным ID

### get - получение определенной записи о сотруднике
Формат использования: -get Id:{Id:int}

Пример: -get Id:123

Возможные ошибки: Нет сотрудника с заданным ID

### getall - получение всех записей о сотрудниках
Формат использования: -getall

Пример: -getall

Возможные ошибки: -

### delete - удаление записи о сотруднике
Формат использования: -delete Id:{Id:int}

Пример: -delete Id:123

Возможные ошибки: Нет сотрудника с заданным ID