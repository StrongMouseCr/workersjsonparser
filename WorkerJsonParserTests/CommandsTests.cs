﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using WorkersJsonParser.Accessory;
using WorkersJsonParser.Commands;
using WorkersJsonParser.Models;
using Xunit;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace WorkerJsonParserTests
{
    public class CommandsTests
    {
        private CommandsWorker _commandsWorker;

        public CommandsTests()
        {
            _commandsWorker = new CommandsWorker();
        }
        
        [Theory]
        [InlineData("First", "Last", "100.5", true)]
        [InlineData("First", null, "100.5", false)]
        public void AddTest(string firstName, string lastName, string salary, bool isOk)
        {
            File.WriteAllText(Paths.OutputPath,"[]");
            
            var args = new Dictionary<string, string>();
            if (firstName is not null)
                args.Add("firstname", firstName);
            if (lastName is not null)
                args.Add("lastname", lastName);
            if (salary is not null)
                args.Add("salary", salary);

            _commandsWorker.Invoke(TypesCommand.Add, args);
            
            var result = JsonSerializer.Deserialize<List<WorkerInfo>>(File.ReadAllText(Paths.OutputPath));
            if (isOk)
            {
                var sample = new WorkerInfo(0, firstName ?? string.Empty, lastName ?? string.Empty,
                    decimal.Parse(salary!, CultureInfo.InvariantCulture));
                Assert.Equal(JsonSerializer.Serialize(sample), JsonSerializer.Serialize(result?[0]));
            }
            else
                Assert.True(result?.Count == 0);
        }
        
        [Theory]
        [InlineData("firstname", "NoFirst")]
        [InlineData("lastname", "NoLast")]
        [InlineData("salary", "0.0")]
        public void UpdateTest(string argKey, string argValue)
        {
            var inputWorker = new []
            {
                new WorkerInfo(0, "First", "Last", (decimal) 123.123) 
            };

            File.WriteAllText(Paths.OutputPath, JsonSerializer.Serialize(inputWorker));
            var args = new Dictionary<string, string>
            {
                {"id", "0"},
                {argKey, argValue}
            };
            _commandsWorker.Invoke(TypesCommand.Update, args);
            var result = JsonSerializer.Deserialize<List<WorkerInfo>>(File.ReadAllText(Paths.OutputPath));

            switch (argKey)
            {
                case "firstname":
                    Assert.Equal(result?[0].FirstName, argValue);
                    break;
                case "lastname":
                    Assert.Equal(result?[0].LastName, argValue);
                    break;
                case "salary":
                    Assert.Equal(result?[0].SalaryPerHour.ToString(CultureInfo.InvariantCulture), argValue);
                    break;
            }
            
        }
    }
}